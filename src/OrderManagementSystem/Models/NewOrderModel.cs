﻿namespace OrderManagementSystem.Models
{
    public class NewOrderModel
    {
        public int ProductId { get; set; }
        public int CustomerId { get; set; }
        public int ProductQuantity { get; set; }
    }
}
