
using Microsoft.EntityFrameworkCore;
using OrderManagementSystem.Data;
using OrderManagementSystem.Data.Entities;
using OrderManagementSystem.Data.Repositories;

namespace OrderManagementSystem
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddDbContext<OmsDbContext>(options =>
                options.UseInMemoryDatabase("MyInMemoryDatabase"));
            builder.Services.AddScoped<ProductRepository>();
            builder.Services.AddScoped<OrderRepository>();
            builder.Services.AddScoped<UnitOfWork>();

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
                // Seed the data
                using (var scope = app.Services.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<OmsDbContext>();
                    SeedData(context);
                }
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }

        private static void SeedData(OmsDbContext context)
        {
            context.Products.AddRange(new Product[]
            {
                new Product(){ Name = "Keyboard", Price = 10 },
                new Product(){ Name = "Mouse", Price = 7.99m },
                new Product(){ Name = "Monitor", Price = 89.99m }
            });

            context.SaveChanges();
        }
    }
}