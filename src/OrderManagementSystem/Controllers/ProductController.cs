﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OrderManagementSystem.Data;
using OrderManagementSystem.Data.Entities;

namespace OrderManagementSystem.Controllers
{
    [Route("api/product")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductByIdAsync([FromServices] UnitOfWork unitOfWork, int id)
        {
            Product product = await unitOfWork.GetProductByIdAsync(id);

            if(product == null)
                return NotFound();
            
            return Ok(product);
        }
    }
}
