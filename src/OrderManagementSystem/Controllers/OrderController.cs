﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OrderManagementSystem.Data;
using OrderManagementSystem.Data.Entities;
using OrderManagementSystem.Models;

namespace OrderManagementSystem.Controllers
{
    [Route("api/order")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        [HttpGet("{id}", Name = nameof(GetOrderByIdAsync))]
        public async Task<IActionResult> GetOrderByIdAsync([FromServices] UnitOfWork unitOfWork, int id) {
            Order order = await unitOfWork.GetOrderByIdAsync(id);

            if(order == null)
                return NotFound();

            return Ok(order);
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrderAsync([FromServices] UnitOfWork unitOfWork,
                                                          [FromBody] NewOrderModel newOrderModel)
        {
            if(newOrderModel.ProductQuantity <= 0)
                return BadRequest("Product Quantity should be greater than zero");

            Product product = await unitOfWork.GetProductByIdAsync(newOrderModel.ProductId);

            if (product == null)
                return NotFound($"Product '{newOrderModel.ProductId}' not found");

            Order createdOrder = unitOfWork.CreateOrder(newOrderModel.CustomerId, product, newOrderModel.ProductQuantity);

            await unitOfWork.SaveChangesAsync();

            return CreatedAtRoute(nameof(GetOrderByIdAsync), new { id = createdOrder.Id }, createdOrder);
        }
    }
}
