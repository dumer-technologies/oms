﻿using OrderManagementSystem.Data.Entities;
using OrderManagementSystem.Data.Repositories;

namespace OrderManagementSystem.Data
{
    public class UnitOfWork
    {
        public OmsDbContext OmsDbContext { get; set; }
        public ProductRepository ProductRepository { get; set; }
        public OrderRepository OrderRepository { get; set; }

        public UnitOfWork(OmsDbContext omsDbContext, ProductRepository productRepository, OrderRepository orderRepository) {
            OmsDbContext = omsDbContext;
            ProductRepository = productRepository;
            OrderRepository = orderRepository;
        }

        public async Task<Product> GetProductByIdAsync(int id)
        {
            return await ProductRepository.GetProductByIdAsync(id);
        }

        public async Task<Order> GetOrderByIdAsync(int id)
        {
            return await OrderRepository.GetOrderByIdAsync(id);
        }

        public Order CreateOrder(int customerId, Product product, int productQuantity)
        {
            return OrderRepository.CreateOrder(customerId, product, productQuantity);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await OmsDbContext.SaveChangesAsync();
        }
    }
}
