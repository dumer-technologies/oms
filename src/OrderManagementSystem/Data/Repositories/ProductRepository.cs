﻿using Microsoft.EntityFrameworkCore;
using OrderManagementSystem.Data.Entities;

namespace OrderManagementSystem.Data.Repositories
{
    public class ProductRepository
    {
        public OmsDbContext OmsDbContext { get; set; }

        public ProductRepository(OmsDbContext omsDbContext)
        {
            OmsDbContext = omsDbContext;
        }

        public async Task<Product> GetProductByIdAsync(int id)
        {
            return await OmsDbContext.Products.FirstOrDefaultAsync(p => p.Id == id);
        }
    }
}
