﻿using Microsoft.EntityFrameworkCore;
using OrderManagementSystem.Data.Entities;

namespace OrderManagementSystem.Data.Repositories
{
    public class OrderRepository
    {
        public OmsDbContext OmsDbContext { get; set; }

        public OrderRepository(OmsDbContext omsDbContext) {
            OmsDbContext = omsDbContext;
        }

        public Order CreateOrder(int customerId, Product product, int productQuantity)
        {
            Order order = new Order()
            {
                CustomerId = customerId,
                Product = product,
                ProductQuantity = productQuantity,
                TotalCost = productQuantity * product.Price
            };

            OmsDbContext.Orders.Add(order);

            return order;
        }

        public async Task<Order> GetOrderByIdAsync(int id)
        {
            return await OmsDbContext.Orders
                                        .Include(o => o.Product)
                                        .FirstOrDefaultAsync(o => o.Id == id);
        }
    }
}
