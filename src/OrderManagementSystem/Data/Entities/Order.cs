﻿namespace OrderManagementSystem.Data.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int ProductQuantity { get; set; }
        public decimal TotalCost { get; set; }
    }
}
