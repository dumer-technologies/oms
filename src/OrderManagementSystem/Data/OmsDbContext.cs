﻿using Microsoft.EntityFrameworkCore;
using OrderManagementSystem.Data.Entities;

namespace OrderManagementSystem.Data
{
    public class OmsDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }

        public OmsDbContext(DbContextOptions<OmsDbContext> dbContextOptions)
            :base(dbContextOptions)
        {
                
        }
    }
}
